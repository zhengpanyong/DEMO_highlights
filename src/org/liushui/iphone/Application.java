package org.liushui.iphone;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import android.util.Log;

public class Application extends android.app.Application {
	public static final String TAG = "BootBroadcastReceiver";
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		registerReceiver(mScreenReceiver, filter);
	}
	private void startService(Context context,  boolean bool) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	private void stopLockScreen(Context context) {
		
	    KeyguardManager km = (KeyguardManager)context.getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);       
	    KeyguardLock kl = km.newKeyguardLock(TAG);  
	    
	    kl.disableKeyguard(); 
	}
	
	BroadcastReceiver mScreenReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

	    	Log.e("zhengpanyong", "BootBroadcastReceiver:" + intent.getAction());
	        if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)){ 
	        	stopLockScreen(context);
	        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
	        	startService(context, true);
	        }
	    
		}
	};
	
}
