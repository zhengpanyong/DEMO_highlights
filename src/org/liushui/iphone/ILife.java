package org.liushui.iphone;

public interface ILife {

	public void onResume();

	public void onPause();

	public void onUpdate();
}