package org.liushui.iphone;

import org.liushui.iphone.BatteryObserver.OnBatteryChange;

import com.example.demo_highlights.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

public class MainActivity extends Activity implements OnBatteryChange {
	BatteryObserver batteryObserver;
	IPhoneLockView root;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lock_iphone_view2);
		root = (IPhoneLockView) findViewById(R.id.root);
		batteryObserver = BatteryObserver.getInstance(this);
		batteryObserver.register();
	}

	protected void onResume() {
		super.onResume();
		batteryObserver.setOnBatteryChange(this);
		root.onResume();
	}

	protected void onPause() {
		super.onPause();
		root.onPause();
	}

	protected void onDestroy() {
		super.onDestroy();
		batteryObserver.unRegister();
	}

	public void onChange(int status, int level, int scale) {
		root.onBattery(status, level, scale);
		root.onUpdate();
	}
	
    //使home物理键失效
    @Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		try {
//			this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("zhengpangyong", "onAttachedToWindow setType err!");
		}
    	Log.e("zhengpangyong", "onAttachedToWindow!");
		super.onAttachedToWindow();
	}
    
    
    
    //使back键，音量加减键失效
   @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return disableKeycode(keyCode, event);
	}
    
    private boolean disableKeycode(int keyCode, KeyEvent event)
    {
    	int key = event.getKeyCode();
    	switch (key)
    	{
		case KeyEvent.KEYCODE_BACK:		
		case KeyEvent.KEYCODE_VOLUME_DOWN:
		case KeyEvent.KEYCODE_VOLUME_UP:
			return true;			
		}
    	return super.onKeyDown(keyCode, event);
    }

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out); 
	}
    
    
}