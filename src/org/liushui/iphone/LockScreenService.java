package org.liushui.iphone;


import android.app.KeyguardManager;
import android.app.Service;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

public class LockScreenService extends Service {
	private SharedPreferences mShare;
	public static final String TAG = "LockScreenService";
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		registerComponent();
	}

	@Override
	public void onDestroy() {
		unregisterComponent();
		//被销毁时启动自身，保持自身在后台存活
      mShare = getSharedPreferences("CallVibrator", Context.MODE_PRIVATE);
      if (mShare.getBoolean("iphoneScreenLock", true)) {
		startService(new Intent(this, LockScreenService.class));
      }
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
//		return super.onStartCommand(intent, flags, startId);
		return Service.START_STICKY;
	}
	
    //注册广播监听
	public void registerComponent()
	{
		// TODO Auto-generated method stub
		IntentFilter mScreenOnOrOffFilter = new IntentFilter();
		mScreenOnOrOffFilter.addAction("android.intent.action.SCREEN_ON");
		mScreenOnOrOffFilter.addAction("android.intent.action.SCREEN_OFF");
		LockScreenService.this.registerReceiver(mScreenOnOrOffReceiver, mScreenOnOrOffFilter);
	}
	
    //解除广播监听
	public void unregisterComponent() 
	{
		if (mScreenOnOrOffReceiver != null)
		{
			LockScreenService.this.unregisterReceiver(mScreenOnOrOffReceiver);
		}
	}
	
	private void startService(Context context,  boolean bool) {
		Intent intent = new Intent(/*context, MainActivity.class*/"android.intent.action.iphoneLockScreen");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	private void stopLockScreen(Context context) {
		
	    KeyguardManager km = (KeyguardManager)context.getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);       
	    KeyguardLock kl = km.newKeyguardLock(TAG);  
	    
	    kl.disableKeyguard(); 
	}
	
	//监听来自用户按Power键点亮点暗屏幕的广播
	private BroadcastReceiver mScreenOnOrOffReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent)
		{
			
            mShare = context.getSharedPreferences("CallVibrator", Context.MODE_PRIVATE);
            if (mShare.getBoolean("iphoneScreenLock", true)) {
	    	Log.e("zhengpanyong", "mScreenOnOrOffReceiver:" + intent.getAction());
	        if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)){ 
	        	stopLockScreen(context);
	        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
	        	startService(context, true);
	        }
            } 
	    
		
			
		}
	};


}
