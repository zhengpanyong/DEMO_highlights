/*
 * Copyright (C) 2012 yueyueniao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.demo_highlights.slidingmenu.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.demo_highlights.R;
import com.example.demo_highlights.slidingmenu.fragment.Rotate3dAnimation.InterpolatedTimeListener;
import com.example.demo_highlights.slidingmenu.fragment.TabSwitcher.OnItemClickLisener;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class PageFragment4 extends Fragment implements OnItemClickListener, OnClickListener{

	GridView mGrid;
	List<Map<String, Object>> myData;
	List<Drawable> myIcon;
	LayoutInflater layoutInflater;
	private boolean enableRefresh;
	private InterpolatedTimeListenerImpl iltl;
	private LinearLayout mLinearLayout1;
	private LinearLayout mLinearLayout2;
	private RelativeLayout mRelativeLayout1;
	
	private RelativeLayout mRelativeLayout2;
	private RelativeLayout mRelativeLayout3;
	private RelativeLayout mRelativeLayout4;
	private RelativeLayout mRelativeLayout5;
	private RelativeLayout mRelativeLayout6;
	private RelativeLayout mRelativeLayout7;
	
	private boolean isFirst = true;
	private boolean enableRefreshText;

	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.page4, null);
		layoutInflater = LayoutInflater.from(getActivity());
		iltl = new InterpolatedTimeListenerImpl();
		mLinearLayout1 = (LinearLayout) view.findViewById(R.id.ll_main);
		mLinearLayout2 = (LinearLayout) view.findViewById(R.id.ll_main_right);
		mRelativeLayout1 = (RelativeLayout) view.findViewById(R.id.rl_mian_more_1);
		mRelativeLayout1.setOnClickListener(this);
		
		mRelativeLayout2 = (RelativeLayout) view.findViewById(R.id.re1);
		mRelativeLayout2.setOnClickListener(this);
		mRelativeLayout3 = (RelativeLayout) view.findViewById(R.id.re2);
		mRelativeLayout3.setOnClickListener(this);
		mRelativeLayout4 = (RelativeLayout) view.findViewById(R.id.re3);
		mRelativeLayout4.setOnClickListener(this);
		mRelativeLayout5 = (RelativeLayout) view.findViewById(R.id.re4);
		mRelativeLayout5.setOnClickListener(this);
		mRelativeLayout6 = (RelativeLayout) view.findViewById(R.id.re5);
		mRelativeLayout6.setOnClickListener(this);
		mRelativeLayout7 = (RelativeLayout) view.findViewById(R.id.re6);
		mRelativeLayout7.setOnClickListener(this);
		
//        mGrid = (GridView) view.findViewById(R.id.gridview);
//        myData = getData();
//        myIcon = getDateImage();
//        mGrid.setAdapter(new AppsAdapter());
//        mGrid.setSelector(new ColorDrawable(Color.TRANSPARENT));
//        mGrid.setOnItemClickListener((OnItemClickListener) this);
		return view;
	}
	
	public void transTo(View view) {
		
	}
	
	public void changeMenu() {
		enableRefresh = true;
		enableRefreshText = true;
//		current_main_more = (ViewGroup) view;
//		mLinearLayout1.setClickable(false);
//
//		float centerX = /*ll_main_right*/mLinearLayout1.getWidth() / 2.0f;
//		float centerY = /*ll_main_right*/mLinearLayout1.getHeight() / 2.0f;
//
//		Rotate3dAnimation rotateAnim = new Rotate3dAnimation(centerX, centerY, false/*Rotate3dAnimation.ROTATE_DECREASE*/, false);
////		rotateAnim.setInterpolatedTimeListener(iltl);
//		rotateAnim.setFillAfter(true);
//		rotateAnim.setDuration(500);
//		
//		/*ll_main_right*/mLinearLayout1.startAnimation(rotateAnim);
		
		float centerX2 = /*ll_main_right*/mLinearLayout2.getWidth() / 2.0f;
		float centerY2 = /*ll_main_right*/mLinearLayout2.getHeight() / 2.0f;

		Rotate3dAnimation rotateAnim2 = new Rotate3dAnimation(centerX2, centerY2, true/*Rotate3dAnimation.ROTATE_DECREASE*/, false);
		rotateAnim2.setInterpolatedTimeListener(iltl);
		rotateAnim2.setFillAfter(true);
		rotateAnim2.setDuration(500);
		
		/*ll_main_right*/mLinearLayout2.startAnimation(rotateAnim2);
		
/*		float centerX3 = ll_main_rightmTextView3.getWidth() / 2.0f;
		float centerY3 = ll_main_rightmTextView3.getHeight() / 2.0f;

		Rotate3dAnimation rotateAnim3 = new Rotate3dAnimation(centerX3, centerY3, trueRotate3dAnimation.ROTATE_DECREASE, true);
//		rotateAnim3.setInterpolatedTimeListener(iltl);
		rotateAnim3.setFillAfter(true);
		rotateAnim3.setDuration(5000);
		
		ll_main_rightmTextView3.startAnimation(rotateAnim3);*/
		
		
	}
	
	private class InterpolatedTimeListenerImpl implements InterpolatedTimeListener {
		public void interpolatedTime(float interpolatedTime) {
			// 监听到翻转进度过半时，更新菜单组容器中要显示的菜单组
//			android.util.Log.e("zhengpanyong", "interpolatedTime:" + interpolatedTime);
			if (enableRefresh && interpolatedTime > 0.5f) {
//				if (current_main_more == rl_main_more_2) {
//					ll_main_right_2.setVisibility(View.GONE);
//					ll_main_right_1.setVisibility(View.VISIBLE);
//					//ll_main_right_1.requestFocus();
//				} else if (current_main_more == rl_main_more_1) {
//					ll_main_right_1.setVisibility(View.GONE);
//					ll_main_right_2.setVisibility(View.VISIBLE);
//					//ll_main_right_2.requestFocus();
//				}
//				mTextView.setText("sadfdsfaffa");
				isFirst = !isFirst;

				
				
				enableRefresh = false;
			}
			if (interpolatedTime >= 1.0f) {
//				mTextView.setClickable(true);
				if (enableRefreshText) {
				if (isFirst) {
					((TextView)mRelativeLayout5.getChildAt(0)).setText("手电筒");
					((TextView)mRelativeLayout6.getChildAt(0)).setText("fragment切换动画");
					((TextView)mRelativeLayout7.getChildAt(0)).setText("自动连接wifi(MD5)");
				} else if (!isFirst) {
					((TextView)mRelativeLayout5.getChildAt(0)).setText("仿奇异UI");
					((TextView)mRelativeLayout6.getChildAt(0)).setText("开心网菜单");
					((TextView)mRelativeLayout7.getChildAt(0)).setText("arc menu");
				}
				enableRefreshText = false;
				}
			}
		}
	}
	
	public void update() {
		((AppsAdapter)mGrid.getAdapter()).notifyDataSetChanged();
	}
	
    protected List<Drawable> getDateImage() {
    	List<Drawable> myIcon = new ArrayList<Drawable>();
    	String[] titles = getActivity().getResources().getStringArray(R.array.demo_names);
    	for (int i = 0; i < titles.length; ++i) {
    		myIcon.add(getDrawableFromInt(i));
    	}
    	
    	return myIcon;
    }
    
    public Drawable getDrawableFromInt(int i) {
    	switch (i % 8) {
    	case 0:
    		return getActivity().getResources().getDrawable(R.drawable.s1);
    	case 1:
    		return getActivity().getResources().getDrawable(R.drawable.s2);
    	case 2:
    		return getActivity().getResources().getDrawable(R.drawable.s3);
    	case 3:
    		return getActivity().getResources().getDrawable(R.drawable.s4);
    	case 4:
    		return getActivity().getResources().getDrawable(R.drawable.s5);
    	case 5:
    		return getActivity().getResources().getDrawable(R.drawable.s6);
    	case 6:
    		return getActivity().getResources().getDrawable(R.drawable.s7);
    	case 7:
    		return getActivity().getResources().getDrawable(R.drawable.s8);
    	default:
    		return getActivity().getResources().getDrawable(R.drawable.s1);
    		
    	}
    }
    
    protected List<Map<String, Object>> getData() {
    	List<Map<String, Object>> myData = new ArrayList<Map<String, Object>>();
    	String[] titles = getActivity().getResources().getStringArray(R.array.demo_names);
    	String[] intents = getActivity().getResources().getStringArray(R.array.demo_intents);
    	for (int i = 0; i < titles.length; ++i) {
    		addItem(myData, titles[i], new Intent(intents[i].toString()));
    	}
        
    	return myData;
    }
    
    protected void addItem(List<Map<String, Object>> data, String name, Intent intent) {
        Map<String, Object> temp = new HashMap<String, Object>();
        temp.put("title", name);
        temp.put("intent", intent);
        data.add(temp);
    }
	
	static class ViewHolder{
		ImageView imageView;
		TextView textView;
	}
    
	 public class AppsAdapter extends BaseAdapter {
		 
			private List<HashMap<Integer, Bitmap>> myBitmap;
			public ArrayList<Integer> mGetBitmapId;
	        public AppsAdapter() {
	    		myBitmap = new ArrayList<HashMap<Integer, Bitmap>>();
	    		mGetBitmapId = new ArrayList<Integer>();
	        }

	        public View getView(int position, View convertView, ViewGroup parent) {
	        	ViewHolder holder = null; 

	            if (convertView == null) {
	            	holder = new ViewHolder();
	            	convertView = layoutInflater.inflate(R.layout.lay1_item, null);
	            	holder.imageView = (ImageView) convertView.findViewById(R.id.grid_imageView);
	            	holder.textView = (TextView) convertView.findViewById(R.id.grid_textView);
//	                i = new ImageView(getActivity());
//	                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//	                imageView.setLayoutParams(new GridView.LayoutParams(100, 100));

	            	convertView.setTag(holder); 
	            } else {
	            	holder = (ViewHolder) convertView.getTag();
	            }
	            
	    		Bitmap bitmap = null;
	    		
/*	    		if (mGetBitmapId.contains(position)) {
	    			for (HashMap<Integer, Bitmap> hp : myBitmap) {
	    				if (hp.containsKey(position)) {
	    					bitmap = hp.get(position);
	    					break;
	    				}
	    			}	
	    		} else {
	    			bitmap = getNewBitMap(myData.get(position).get("title").toString());
	    			mGetBitmapId.add(position);
	    			addBitmapItem(myBitmap, position, bitmap);
	    		}*/
	    		bitmap = getNewBitMap(myData.get(position).get("title").toString());
            	holder.imageView.setImageBitmap/*setImageDrawable*/(bitmap);
//            	holder.textView.setText(myData.get(position).get("title").toString());

//	            ResolveInfo info = mApps.get(position);
	            
//	            i.setImageDrawable(myIcon.get(position)/*getActivity().getResources().getDrawable(R.drawable.ic_launcher)*//*info.activityInfo.loadIcon(getPackageManager())*/);

	            return convertView;
	        }

	        protected void addBitmapItem(List<HashMap<Integer, Bitmap>> data, int position, Bitmap bitmap) {
	        	HashMap<Integer, Bitmap> temp = new HashMap<Integer, Bitmap>();
	            temp.put(position, bitmap);
	            data.add(temp);
	        }
	        
	    	public Bitmap getNewBitMap(String text) {
	    		Bitmap bmp = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.t3);
	            Bitmap newBitmap = Bitmap.createBitmap(bmp.getWidth(),bmp.getHeight(), Config.RGB_565/*ARGB_4444*/);
	            Canvas canvas = new Canvas(newBitmap);
	            canvas.drawBitmap(bmp, 0, 0, null);
	            TextPaint textPaint = new TextPaint();
	            textPaint.setColor(Color.WHITE);
	            textPaint.setAntiAlias(true);
	            textPaint.setTextSize(16.0F * 4);
	            StaticLayout sl= new StaticLayout(text, textPaint, newBitmap.getWidth()-8, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
	            canvas.translate(6, 40 + 160);
	            sl.draw(canvas);
	            return newBitmap;
	        }
	        
	        public final int getCount() {
	            return myData.size();
	        }

	        public final Object getItem(int position) {
	            return myData.get(position);
//	        	return null;
	        }

	        public final long getItemId(int position) {
	            return position;
	        }
	    }

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onItemClick(AdapterView<?> Gridview, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
        Map<String, Object> map = (Map<String, Object>)Gridview.getItemAtPosition(position);

        Intent intent = (Intent) map.get("intent");
        startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rl_mian_more_1:
			changeMenu();
			break;
		case R.id.re1:
	        Intent intent = new Intent("android.intent.action.lockScreenSettings");
	        startActivity(intent);
			break;
		case R.id.re2:
	        Intent intent2 = new Intent("android.intent.action.FileExplorer");
	        startActivity(intent2);
			break;
		case R.id.re3:
	        Intent intent3 = new Intent("android.intent.action.anzhuobaodian");
	        startActivity(intent3);
			break;
		case R.id.re4:
			Intent intent4;
			if (isFirst) {
				intent4 = new Intent("android.intent.action.light");//36.手电筒
			} else {
				intent4 = new Intent("android.intent.action.myqiyi");//37.仿奇异UI
			}
	        startActivity(intent4);
			break;
		case R.id.re5:
			Intent intent5;
			if (isFirst) {
				intent5 = new Intent("android.intent.action.jazzyviewpager");//38.fragment切换动画
			} else {
				intent5 = new Intent("android.intent.action.KaiXinMenuMainActivity");//43.开心网菜单
			}
	        startActivity(intent5);
			break;
		case R.id.re6:
			Intent intent6;
			if (isFirst) {
				intent6 = new Intent("android.intent.action.AutoConn");//自动连接wifi(MD5)
			} else {
				intent6 = new Intent("android.intent.action.arcmenu");//29.arc menu
			}
	        startActivity(intent6);
			break;
		default:
			break;
		}
	}

}

