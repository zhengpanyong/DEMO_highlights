package com.example.demo_highlights.slidingmenu.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.demo_highlights.R;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CoverFlowImageAdapter extends BaseAdapter {
	private Context mContext;
	private Integer[] mImageIds = null;
	private Class<?>[] target=null;
	private LayoutInflater layoutInflater;
	private List<Map<String, Object>> myData;
	private List<HashMap<Integer, Bitmap>> myBitmap;
	public ArrayList<Integer> mGetBitmapId;
	private Boolean mBusy = false;
	Bitmap mBitmap;
	Bitmap bmpBase;

	public Integer[] getmImageIds() {
		return mImageIds;
	}

	public Class<?>[] getTarget() {
		return target;
	}

	public CoverFlowImageAdapter(Context c,Integer[] mImageIds,Class<?>[] target) {
		mContext = c;
		this.mImageIds=mImageIds;
		this.target=target;
		layoutInflater = LayoutInflater.from(c);
		myBitmap = new ArrayList<HashMap<Integer, Bitmap>>();
		mGetBitmapId = new ArrayList<Integer>();
	}
	
	public CoverFlowImageAdapter(Context c,List<Map<String, Object>> data) {
		mContext = c;
		myData = data;
		layoutInflater = LayoutInflater.from(c);
		myBitmap = new ArrayList<HashMap<Integer, Bitmap>>();
		mGetBitmapId = new ArrayList<Integer>();
		bmpBase = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.t3);
//		mBitmap = createReflectedBitmap(getNewBitMap("load..."));
		mBitmap = createReflectedBitmap(bmpBase);
		
	}
	
	public void setBusy(Boolean bool) {
		
		if (!bool && mBusy) {
			mBusy = bool;
			notifyDataSetChanged();
		} else {
			mBusy = bool;
		}
		
		
	}

	@Override
	public int getCount() {
		return myData.size();//mImageIds.length;
	}

	@Override
	public Object getItem(int position) {
//		return position;
		return myData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	static class ViewHolder{
		ImageView imageView;
		TextView textView;
	}
	
    protected void addBitmapItem(List<HashMap<Integer, Bitmap>> data, int position, Bitmap bitmap) {
    	HashMap<Integer, Bitmap> temp = new HashMap<Integer, Bitmap>();
        temp.put(position, bitmap);
        data.add(temp);
    }
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		ImageView i = new ImageView(mContext);//createReflectedImages(mContext,mImageIds[position]);
//		
//		Bitmap bitmap = null;
		
/*		if (mGetBitmapId.contains(position)) {
			for (HashMap<Integer, Bitmap> hp : myBitmap) {
				if (hp.containsKey(position)) {
					bitmap = hp.get(position);
					break;
				}
			}	
		} else {
			bitmap = createReflectedBitmap(getNewBitMap(myData.get(position).get("title").toString()));
			mGetBitmapId.add(position);
			addBitmapItem(myBitmap, position, bitmap);
		}*/
//		if (!mBusy) {
//			bitmap = createReflectedBitmap(getNewBitMap(myData.get(position).get("title").toString()));
//		} else {
//			bitmap = mBitmap;
//		}
//		i.setImageBitmap(bitmap);
//		i.setLayoutParams(new CoverFlow.LayoutParams(120, 100));
//		i.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//		// ���õĿ����,��ֹͼ������ת��ʱ����־��
//		BitmapDrawable drawable = (BitmapDrawable) i.getDrawable();
//		drawable.setAntiAlias(true);
		
    	ViewHolder holder = null; 

        if (convertView == null) {
        	holder = new ViewHolder();
        	convertView = layoutInflater.inflate(R.layout./*fragment_gallery_item*/lay1_item, null);
//        	convertView.setLayoutParams(new CoverFlow.LayoutParams(120, 100));
        	convertView.setBackgroundColor(0x00FFFFFF);
        	holder.imageView = (ImageView) convertView.findViewById(R.id.grid_imageView);
        	holder.textView = (TextView) convertView.findViewById(R.id.grid_textView);
//            i = new ImageView(getActivity());
//            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));

        	convertView.setTag(holder); 
        } else {
        	holder = (ViewHolder) convertView.getTag();
        }
//    	holder.imageView.setImageBitmap(createReflectedImages(mContext,mImageIds[position]));
//        holder.imageView.setLayoutParams(new RelativeLayout.LayoutParams(120, 100));
//        holder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    	holder.imageView.setImageBitmap(/*getNewBitMap("" + position)*/mBitmap);
		BitmapDrawable drawable = (BitmapDrawable) holder.imageView.getDrawable();
		drawable.setAntiAlias(true);
    	holder.textView.setText(myData.get(position).get("title").toString()/*"" + position*/);
//    	holder.textView.setTextSize(holder.textView.getTextSize());

//        ResolveInfo info = mApps.get(position);
        
//        i.setImageDrawable(myIcon.get(position)/*getActivity().getResources().getDrawable(R.drawable.ic_launcher)*//*info.activityInfo.loadIcon(getPackageManager())*/);

        return convertView;
//		return i;
	}
	
	public Bitmap getNewBitMap(String text) {
		Bitmap bmp = bmpBase;//BitmapFactory.decodeResource(mContext.getResources(), R.drawable.t3);
        Bitmap newBitmap = Bitmap.createBitmap(bmp.getWidth(),bmp.getHeight(), Config.RGB_565/*ARGB_4444*/);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawBitmap(bmp, 0, 0, null);
        TextPaint textPaint = new TextPaint();
        textPaint.setColor(Color.WHITE);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(16.0F * 4);
        StaticLayout sl= new StaticLayout(text, textPaint, newBitmap.getWidth()-8, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
        canvas.translate(6, 40 + 160);
        sl.draw(canvas);
        return newBitmap;
    }

	public float getScale(boolean focused, int offset) {
		return Math.max(0, 1.0f / (float) Math.pow(2, Math.abs(offset)));
	}
	
	public Bitmap createReflectedBitmap(Bitmap bitmap) {

		Bitmap originalImage = bitmap;
		final int reflectionGap = 4;
		int width = originalImage.getWidth();
		int height = originalImage.getHeight();

		Matrix matrix = new Matrix();
		matrix.preScale(1, -1);

		Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
				height / 2, width, height / 2, matrix, false);

		Bitmap bitmapWithReflection = Bitmap.createBitmap(width,
				(height + height / 2), Config.ARGB_8888);

		Canvas canvas = new Canvas(bitmapWithReflection);
		canvas.drawBitmap(originalImage, 0, 0, null);

		Paint deafaultPaint = new Paint();
		canvas.drawRect(0, height, width, height + reflectionGap, deafaultPaint);
		canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);

		Paint paint = new Paint();
		LinearGradient shader = new LinearGradient(0, originalImage
				.getHeight(), 0, bitmapWithReflection.getHeight()
				+ reflectionGap, 0x70ffffff, 0x00ffffff, TileMode.MIRROR);

		paint.setShader(shader);
		paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
		canvas.drawRect(0, height, width, bitmapWithReflection.getHeight()
				+ reflectionGap, paint);

//		ImageView imageView = new ImageView(mContext);
//		imageView.setImageBitmap(bitmapWithReflection);

//		return imageView;
		return bitmapWithReflection;
	
	}
	
	/**
	 * ΪͼƬ��ӵ�Ӱ
	 * @param mContext
	 * @param imageId
	 * @return
	 */
	public /*ImageView*/Bitmap createReflectedImages(Context mContext,int imageId) {
		Bitmap originalImage = BitmapFactory.decodeResource(mContext.getResources(), imageId);
		final int reflectionGap = 4;
		int width = originalImage.getWidth();
		int height = originalImage.getHeight();

		Matrix matrix = new Matrix();
		matrix.preScale(1, -1);

		Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
				height / 2, width, height / 2, matrix, false);

		Bitmap bitmapWithReflection = Bitmap.createBitmap(width,
				(height + height / 2), Config.ARGB_8888);

		Canvas canvas = new Canvas(bitmapWithReflection);
		canvas.drawBitmap(originalImage, 0, 0, null);

		Paint deafaultPaint = new Paint();
		canvas.drawRect(0, height, width, height + reflectionGap, deafaultPaint);
		canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);

		Paint paint = new Paint();
		LinearGradient shader = new LinearGradient(0, originalImage
				.getHeight(), 0, bitmapWithReflection.getHeight()
				+ reflectionGap, 0x70ffffff, 0x00ffffff, TileMode.MIRROR);

		paint.setShader(shader);
		paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
		canvas.drawRect(0, height, width, bitmapWithReflection.getHeight()
				+ reflectionGap, paint);

//		ImageView imageView = new ImageView(mContext);
//		imageView.setImageBitmap(bitmapWithReflection);

//		return imageView;
		return bitmapWithReflection;
	}
	
}
